# -*- coding: utf-8 -*-
from scrapy.contrib.djangoitem import DjangoItem
from core.models import ArticleItems

class ArticleScrapyItem(DjangoItem):
    django_model = ArticleItems
