# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from nltk import clean_html
from nlp_utils.flatte import flatten_this

from ..items import ArticleScrapyItem


class Tvn24Spider(CrawlSpider):
    """Spider for tvn24"""

    name = 'tvn24'
    plain_name = 'tvn24'

    allowed_domains = ['tvn24.pl', 'www.tvn24.pl']
    start_urls = ['http://www.tvn24.pl']

    rules = [Rule(SgmlLinkExtractor(
        allow=['s\.html$']), 'article_parse')]

    def article_parse(self, response):

        sel = Selector(response)
        article = ArticleScrapyItem()

        article['url'] = response.url
        article['title'] = ' '.join(sel.xpath("//*[@id='tvn24']/div[2]/div[2]/div/div[2]/article/h1").extract())
        article['content'] = ' '.join(sel.xpath("//*[@id='tvn24']/div[2]/div[2]/div/div[2]/article").extract())
        article['page'] = self.name

        # remove html tags
        article['title'] = clean_html(article['title'])
        article['content'] = clean_html(article['content'])

        article['flatten'] = flatten_this(article['content'])

        return article
