# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from nltk import clean_html
from nlp_utils.flatte import flatten_this

from ..items import ArticleScrapyItem

class OnetSpider(CrawlSpider):
    name = 'onet'
    plain_name = 'Onet.pl'
    allowed_domains = ['onet.pl', 'wiadomosci.onet.pl']
    start_urls = ['http://www.onet.pl/']

    rules = [Rule(SgmlLinkExtractor(
        allow=['/kraj|religia|nauka|ciekawostki|tylko-w-wonecie|swiat|lublin|newsy/\w+']),
                  'article_parse')
    ]

    def article_parse(self, response):

        sel = Selector(response)
        article = ArticleScrapyItem()

        article['url'] = response.url
        article['title'] = ' '.join(sel.xpath("//*[@id='mainTitle']/h1").extract())
        article['content'] = ' '.join(sel.xpath("//*[@id='main']/div[1]").extract())
        article['page'] = self.name

        # remove html tags
        article['title'] = clean_html(article['title'])
        article['content'] =  clean_html(article['content'])

        article['flatten'] = flatten_this(article['content'])

        return article
