# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.http import Request
from nltk import clean_html

from ..items import ArticleScrapyItem
from nlp_utils.flatte import flatten_this



class KurierLubelskiSpider(CrawlSpider):
    """Spider for Kurier Lubelski"""

    name = 'kurier'

    plain_name = 'Kurier Lubelski'
    allowed_domains = ['kurierlubelski.pl']
    start_urls = ['http://www.kurierlubelski.pl']
    rules = [Rule(SgmlLinkExtractor(allow=['/artykul/\w+']), 'article_parse')]

    def article_parse(self, response):

        sel = Selector(response)
        article = ArticleScrapyItem()

        article['url'] = response.url
        article['title'] = ' '.join(sel.xpath("//*[@id='material']/header/h1").extract())
        article['content'] = ' '.join(sel.xpath("//*[@id='tresc']").extract())
        article['page'] = self.plain_name = 'kurier'

        # remove html tag
        article['title'] = clean_html(article['title'])
        article['content'] = clean_html(article['title']) + clean_html(article['content'])

        article['flatten'] = flatten_this(article['content'])

        return article