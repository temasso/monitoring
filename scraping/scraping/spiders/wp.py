# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from nltk import clean_html
from nlp_utils.flatte import flatten_this
from ..items import ArticleScrapyItem




class KurierLubelskiSpider(CrawlSpider):
    """Spider for Kurier Lubelski"""

    name = 'wp'

    allowed_domains = ['wiadomosci.wp.pl', 'http://wiadomosci.wp.pl/?ticaid=112b70', 'http://wiadomosci.wp.pl/.?ticaid=112b70']
    start_urls = ['http://wiadomosci.wp.pl/', 'http://wiadomosci.wp.pl/?ticaid=112b70', 'http://wiadomosci.wp.pl/.?ticaid=112b70']

    rules = [Rule(SgmlLinkExtractor(
        allow=['wiadomosc/\.html$']), 'article_parse')]

    def article_parse(self, response):

        sel = Selector(response)
        article = ArticleScrapyItem()

        article['url'] = response.url
        article['title'] = ' '.join(sel.xpath("//*[@id='bxArtykul']/h1").extract())
        article['content'] = ' '.join(sel.xpath("//*[@id='intertext1']").extract())
        article['page'] = self.name

        # clean html
        article['title'] = clean_html(article['title'])
        article['content'] = clean_html(article['title']) + clean_html(article['content'])

        article['flatten'] = flatten_this(article['content'])

        return article


