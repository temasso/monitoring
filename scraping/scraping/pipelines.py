# -*- coding: utf-8 -*-
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from scrapy import log
from core.models import ArticleItems



class ScrapingPipeline(object):
    def process_item(self, item, spider):

        scraped_items = ArticleItems.objects.filter(url__iexact=item['url'])
        if scraped_items.exists():
            log.msg("** Already scraped URL", level=log.DEBUG)
        else:
            item.save()

        return item



