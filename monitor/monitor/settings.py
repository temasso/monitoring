# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
from celery.schedules import crontab

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SETTINGS_DIR = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(SETTINGS_DIR, os.pardir)
PROJECT_PATH = os.path.abspath(PROJECT_PATH)

TEMPLATE_PATH = os.path.join(PROJECT_PATH, 'templates')
STATIC_PATH = os.path.join(PROJECT_PATH, 'static')

ADMINS = (
     ('Monitor', 'monitor@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'monitor',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'monitorus',
        'PASSWORD': 'monitor',
        'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',
    },

}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
TIME_ZONE = 'Europe/Warsaw'

LANGUAGE_CODE = 'pl'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
#TIME_ZONE = 'UTC'
USE_TZ = False

MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(PROJECT_PATH, '/static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
   STATIC_PATH,
)


TEMPLATE_DIRS = [
    os.path.join(BASE_DIR, 'templates')
]
TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "core.context.zapytania"
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'y90r^vtui)lradz@=ujpz150+7&pcvw4hb^bksdt#s!%6kfem6'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'monitor.urls'

WSGI_APPLICATION = 'monitor.wsgi.application'

TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\','/'),)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django_extensions',
    'celery',
    'south',
    'core',
)

WSZYSTKIE_WARIANTY = True
PROG = 51.0

LOGIN_URL = '/accounts/login/'
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
CELERY_RESULT_BACKEND = 'amqp'
CELERYD_MAX_TASKS_PER_CHILD = 1
BROKER_URL = 'amqp://guest:guest@localhost:5672/'

CELERYBEAT_SCHEDULE = {
    'crawlers': {
        'task': 'core.tasks.run_crawlers',
        'schedule': crontab(minute='*/60'),
        'args': (),
       'kwargs': {},
    },
    'user-query': {
        'task': 'core.tasks.analyze_new_articles',
        'schedule': crontab(minute='*/50'),
        'args': (),
        'kwargs': {},
    },
    'stemming': {
        'task': 'core.tasks.stem_query_keyword',
        'schedule': crontab(minute='*/50'),
        'args': (),
        'kwargs': {},
    }
}

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
