# -*- coding: utf-8 -*-
from __future__ import absolute_import
import re
import logging
import datetime
import subprocess
from ast import literal_eval
from monitor import celery_app as app
from nlp_utils.stemming import stem_words_list
from core.models import ArticleItems, UserQuery

logger = logging.getLogger(__name__)

"""
celery -B -A monitor worker -l info
"""

@app.task
def run_crawlers():
    """ run crawlers """

    logger.warning("Uruchamianie pająków")
    ku = subprocess.Popen(['scrapy', 'crawl', 'kurier' ], cwd='/home/temasso/monitor_projekt/monitoring/scraping')
    tvn = subprocess.Popen(['scrapy', 'crawl', 'tvn24' ], cwd='/home/temasso/monitor_projekt/monitoring/scraping')
    onet = subprocess.Popen(['scrapy', 'crawl', 'onet' ], cwd='/home/temasso/monitor_projekt/monitoring/scraping')
    date = datetime.datetime.now()
    logger.info("Ukończono zadanie RUN CRAWLERS - {}".format(date))

@app.task
def stem_query_keyword():
    """ stem polish keywords """

    unprocessed = UserQuery.objects.filter(processed=False)
    if unprocessed.count() == 0:
        logger.info("Wszystkie zapytania uzytkowników są już przetworzone")
    else:
        logger.info("%d pozostało zapytań do przetworzenia" % unprocessed.count())

    for query in unprocessed:
        query_keywords = literal_eval(query.key_words)
        try:
            try:
                stemmed = stem_words_list(query_keywords)
            except:
                logger.error("Nie można utworzyć rdzenia dla %s" % query.key_words)
            try:
                query.normalized_key_word = unicode(stemmed)
                query.processed = True
                query.save()
            except:
                logger.error("Nie mozna zapisać wartości unicode(stemmed) do pola normalized_key_word")
        except:
            logger.error("Wystąpił problem w procesie stemowania słów dla %s" %  str(query))



def check_query_keywords_in_article(query, article):
    """sprawdz konkretne zapytanie w danym artukule"""

    FOUND_IN_ARTICLE = False # gdy znaleziono cokolwiek
    founded_stat = {} # {slowo: ile_razy, ...}

    slowa_kluczowe = literal_eval(query.normalized_key_word)  # lista stemowanych slow kluczowych
    logger.info("Slowa kluczowe: %s" % query.normalized_key_word)
    ile_slow_kluczowych = len(slowa_kluczowe)
    tekst_artykulu = article.flatten

    for slowo in slowa_kluczowe:
        counter = 0
        for m in re.finditer(slowo, tekst_artykulu):
            FOUND_IN_ARTICLE = True
            founded_stat[slowo] = counter + 1
            logger.info("Znaleziono slowo: %s w art.: %s" % (slowo, article))

    if FOUND_IN_ARTICLE:
        logger.info("Znaleziono artykul %d dla zapytania %s" % (article.pk, query.query_name))
        percentage = ((len(founded_stat) + 0.0) / ile_slow_kluczowych) * 100
        percentage = round(percentage, 2)
        try:
            # dopisz artykul do zapytania
            new = query.medium.create(article=article, stat=unicode(founded_stat), percentage=percentage)
            new.save()
        except:
            logger.error("Relacja %s z %s już istnieje!" % query, article[:30])

def check_article(article, queries):
    # sprawdz wszystkie zapytania dla tego artykulu
    for query in queries:
        logger.info("Sprawdzam query: %s" % query)
        # czy artykul odpowiada na to zapytanie uzytkownika
        check_query_keywords_in_article(query=query, article=article)

@app.task
def analyze_new_articles():
    unprocessed = UserQuery.objects.filter(processed=False)
    if unprocessed.exists():
        logger.warning("Nie wszystkie zapytania przetworzone {}".format(unprocessed))
        return
    # pobierz jednokrotnie do 10 artykułów
    new_articles_set = ArticleItems.objects.filter(analyzed=False)[:10]
    queries = UserQuery.objects.filter(processed=True)

    if not new_articles_set.exists():
        logger.info("Narazie BRAK nowych artykułów")

    for article in new_articles_set:
        # sprawdz kazdy artykul
        logger.info("Sprawdzam artykul : %s" % article.title[:30])
        check_article(article, queries)
        logger.info("Sprawdzony: %s" % article.title[:30])
        article.analyzed = True
        article.save()






