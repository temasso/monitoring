# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.widgets import AdminURLFieldWidget
from django.db.models import URLField
from django.utils.safestring import mark_safe


from .models import ArticleItems, UserQuery, QueryRelatedArticles

class URLFieldWidget(AdminURLFieldWidget):

    def render(self, name, value, attrs=None):
        widget = super(URLFieldWidget, self).render(name, value, attrs)
        return mark_safe(u'%s&nbsp;&nbsp;<input  style="float:right; margin-right:300px;" class="btn btn-success" type="button" '
                         u'value="Przejdz do strony" onclick="window.'
                         u'open(document.getElementById(\'%s\')'
                         u'.value)" />' % (widget, attrs['id']))

class MyAdmin(admin.ModelAdmin):
    formfield_overrides = {URLField: {'widget': URLFieldWidget}, }
    date_hierarchy = 'scraped_date'
    list_display = ('pk', 'short_title', 'scraped_date', 'page', 'analyzed')
    ordering = ('-scraped_date',)


class UserQueryAdmin(admin.ModelAdmin):
    list_display = ('user', 'query_name', 'processed', 'date')
    date_hierarchy = 'date'


class QueryRelatedArticlesAdmin(admin.ModelAdmin):
    list_display = ('query', 'article', 'percentage', 'date')


admin.site.register(ArticleItems, MyAdmin)
admin.site.register(UserQuery, UserQueryAdmin)
admin.site.register(QueryRelatedArticles, QueryRelatedArticlesAdmin)
