# -*- coding: utf-8 -*-
def zapytania(request):
    user = request.user
    if not user.is_anonymous():
        queries = user.queries.filter(processed=True)
        return {'queries': queries, }
    else:
        return {}



