# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from views import PanelView, QueryDetail, NewQueryView, receive_keyword

urlpatterns = patterns('',
    url(r'^$', PanelView.as_view(), name='panel'),
    url(r'^q/(?P<pk>\d+)/$', QueryDetail.as_view(), name='query_detail'),
    url(r'^n/$', NewQueryView.as_view(), name='new'),
    url(r'^r/$', receive_keyword, name='receive')  # post ajax
)
