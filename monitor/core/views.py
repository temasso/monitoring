# -*- coding: utf-8 -*-
import json
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponse

from models import UserQuery, ArticleItems


class PanelView(TemplateView):
    template_name = 'core/panel.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PanelView, self).dispatch(*args, **kwargs)

class NewQueryView(TemplateView):
    template_name = 'core/new_query.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NewQueryView, self).dispatch(*args, **kwargs)

@login_required
@csrf_protect
def receive_keyword(request):

    if request.POST and request.is_ajax():
        slowa_kluczowe = request.POST.getlist('slowa_kluczowe')
        nazwa = request.POST['nazwa']
        if not nazwa:
            slow = {'status':0, 'text': u"Podaj nazwę zapytania"}
            return HttpResponse(json.dumps(slow), mimetype='application/json')

        if len(slowa_kluczowe) < 1:
            slow = {'status': 0, 'text': u"Wprowadź słowa kluczowe"}
            return HttpResponse(json.dumps(slow), mimetype='application/json')

        keywords_u = unicode(slowa_kluczowe)
        keywords = keywords_u.replace("u'","'")
        q = UserQuery.objects.create(user=request.user, query_name=nazwa, key_words=keywords)
        q.save()

        response_dict = {
            'url': reverse('query_detail', kwargs={'pk': q.pk}),
            'status': 1
        }
        return HttpResponse(json.dumps(response_dict), mimetype='application/json')
    else:
        return HttpResponse("Must be an Ajax")

class QueryDetail(DetailView):
    model = UserQuery
    template_name = 'core/query_detail.html'
    context_object_name = 'query'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QueryDetail, self).dispatch(*args, **kwargs)