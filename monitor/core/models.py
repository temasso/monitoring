# -*- coding: utf-8 -*-
from ast import literal_eval
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class ArticleItems(models.Model):
    """Basic article obtained from Scrapy"""

    url = models.URLField(unique=True, primary_key=False,
                          verbose_name=u"Adres URL")

    title = models.TextField(verbose_name=u"Tytuł")
    content = models.TextField(u"Treść artykułu")
    page = models.CharField(u"Portal", max_length=40)
    scraped_date = models.DateTimeField(auto_now_add=True,
                                        verbose_name=u"Data pozyskania")

    flatten = models.TextField(default='empty',
                                 verbose_name=u"Tekst uproszczony")

    analyzed = models.BooleanField(default=False,
                                   verbose_name=u"Status analizy")

    def __unicode__(self):
        return "%s - %s" % (self.title, self.page)

    def short_title(self):
        return self.title[:40] + '...'

    class Meta:
        db_table = "articles"
        verbose_name_plural = u"Artykuły"
        ordering = ['scraped_date']


class UserQuery(models.Model):
    """Custom user query"""

    user = models.ForeignKey(User, related_name='queries')
    query_name = models.CharField(max_length=100,
                                  verbose_name=u"Nazwa zapytania")
    processed = models.BooleanField(u"Status",
                                    default=False)
    date = models.DateTimeField(auto_now_add=True,
                            verbose_name=u"Data dodania")

    #NLP utils
    key_words = models.TextField(verbose_name=u"Słowa kluczowe")
    normalized_key_word = models.TextField(blank=True, null=True,
                                           verbose_name=u"Przetworzone słowa kluczowe")

    articles = models.ManyToManyField(ArticleItems, through='QueryRelatedArticles',
                                      blank=True, null=True)

    def __unicode__(self):
        return "%s - %s" % (self.user, self.query_name)

    @property
    def has_new(self):
        related_new = self.medium_filtered
        return related_new.count()

    @property
    def medium_filtered(self):
        prog = getattr(settings, 'PROG', 51.0)
        return self.medium.filter(percentage__gte=prog)

    @property
    def utc_keywords(self):
        if self.key_words:
            klucze = literal_eval(self.key_words)
            return ','.join(klucze)
        return u"brak słów kluczowych"

    @property
    def normalized_as_list(self):
        return literal_eval(self.normalized_key_word)

    @property
    def utc_normalized(self):
        if self.normalized_key_word:
            klucze = literal_eval(self.normalized_key_word)
            return ','.join(klucze)
        return 'słowa nieprzetworzone'

    def get_absolute_url(self):
        return "/q/%d" % self.pk

    def count_keywords(self):
        lista = literal_eval(self.normalized_key_word)
        return len(lista)

    class Meta:
        db_table = 'queries'
        verbose_name_plural = u"Zapytania użytkowników"
        ordering = ['date']


class QueryRelatedArticles(models.Model):
    """Intermediary Table"""
    article = models.ForeignKey(ArticleItems, related_name='related_ar')
    query = models.ForeignKey(UserQuery, related_name='medium')

    stat = models.CharField(u"Statystyka trafności", max_length=500, default=' ')
    percentage = models.FloatField(u"Procent trafności dla słów", default=0.0, blank=True, null=True)

    date = models.DateTimeField(u"Data powiązania", auto_now_add=True)

    def __unicode__(self):
        return "%s - %s" % (self.query, self.article)

    class Meta:
        db_table = 'related'
        unique_together=("article", "query")
        verbose_name_plural = u"Powiązane artykuły"

