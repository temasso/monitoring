#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "monitor.settings")
    sys.path.append('/home/temasso/monitor_projekt/monitoring/monitor')

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
