# -*- coding: utf-8 -*-
import os
import sys
import logging
from django.conf import settings
logger = logging.getLogger(__name__)

plik_slownika = 'stem_UTF-8s'
f_path = os.path.abspath(plik_slownika)

def stem_word(word):
    sys.path.append(os.path.dirname(__file__))
    logger.info("STEM_WORD: Stemuję slowo: %s " % word)
    ZNALEZIONO = False
    COUNT = 0
    word = word.lower()
    szukane_p = ',' + word + ','  # workaround dzięki któremu linii nie trzeba przetwarzać na listę Pythona
    szukane_n = ',' + word + '\r'
    logger.info("STEM_WORD: Szukane %s" % szukane_p)
    with open(f_path, 'r') as slownik:
        stem = []
        COUNT += 1
        for linia in slownik:
            koduj = linia.lower()
            if (szukane_p in koduj) or (szukane_n in koduj):
                ZNALEZIONO = True
                st = koduj.split(',')[0]
                logger.info("Znaleziono dla %s - %s" % (word, st))
                stem.append(st)

        slownik.close()

    logger.info("Ilosć różnych wariantów słowa: %d" % COUNT)
    if ZNALEZIONO:
        if getattr(settings, 'WSZYSTKIE_WARIANTY', True):
            logger.info("STEM_WORD:: WARIANT Znaleziono stem: %s" % min(list(set(stem)), key=len))
            return list(set(stem))
        else:
            logger.info("STEM_WORD: Znaleziono stem: %s" % min(list(set(stem)), key=len))
            return min(list(set(stem)), key=len) # unikalne
    else:
        logger.info("STEM_WORD: NIE Znaleziono dla %s" % (word))
        return(word)


def stem_words_list(words):
    output = []
    for word in words:
        try:
            post_stem = stem_word(word=word)
        except:
            logger.warning("Problem stemmingu slowa: %s" % word)

        if isinstance(post_stem, list): # gdy zawiera więcej niż jeden wariant
            for el in post_stem:
                output.append(el)
        else:
            output.append(post_stem)

    return output

