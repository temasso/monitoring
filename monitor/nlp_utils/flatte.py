# -*- coding: utf-8 -*-
import re

POPULAR = (
    ' po ',
    ' w ',
    ' na ',
    ' z ',
    ' do ',
    ' dla ',
    ' i ',
    ' za ',
    ' nie ',
    ' dla ',
    ' jest ',
    ' tam ',
    ' o ',
)

ZNAKI = (
    '.',
    '!',
    '?',
    '"',
    "'",
    "/",
    ";",
    ",",
    "[",
    "]",
    "{",
    "}",
    ":",
    "(",
    "(",
    "- ",
    " -",
    " - ",
)

def remove_popular_word(text):
    """ usuwa najczestsze wyrazy """
    texted = text
    for slowo in POPULAR:
        texted = texted.replace(slowo, " ")

    return texted

def prepare_text(text):
    """ usuwa interpunkcje, zmienia na male litery """

    texted = text.lower() # male litery
    texted = remove_popular_word(texted) # partykuly itp.
    for znak in ZNAKI:
        texted = texted.replace(znak, " ") # znaki interpunkcyjne

    texted = re.sub('\s+', ' ', texted).strip()  # pojedyncze biale znaki

    return texted

def flatten_this(text):
    prepared = prepare_text(text)
    return prepared


